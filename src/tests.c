#include "tests.h"
#define RANDOM_INIT_SIZE 100
#define BLOCK_SIZE 400
#define REGION_MIN_SIZE 8192

void print_padding(){
    printf("------------------------\n\n\n\n");
}

void print_test_successful(){
    printf("The test was successful!\n");
}

void free_heap(void * heap, size_t size){
    munmap(heap, size);
}

void allocate_single_block(){
    void * heap = heap_init(RANDOM_INIT_SIZE);
    printf("Allocate and free single block.\n\n");
    void * block = _malloc(24);
    debug_heap(stdout, heap);
    _free(block);
    debug_heap(stdout, heap);
    print_padding();
    free_heap(heap, REGION_MIN_SIZE);
    print_test_successful();
}

void merge_all_blocks(){
    void * heap = heap_init(RANDOM_INIT_SIZE);
    printf("Merge all blocks.\n\n");
    void * blocks[10] = {0};
    for (int i = 0; i < 10; i++){
        blocks[i] = _malloc(BLOCK_SIZE);
    }
    debug_heap(stdout, heap);
    for (int i = 1; i < 10; i++){
        _free(blocks[i]);
    }
    debug_heap(stdout, heap);
    _free(blocks[0]);
    debug_heap(stdout, heap);
    print_padding();
    free_heap(heap, REGION_MIN_SIZE);
    print_test_successful();
}

void simple_tests(){
    allocate_single_block();
    merge_all_blocks();
}

void grow_near(){
    void * heap = heap_init(RANDOM_INIT_SIZE);
    printf("Heap grow after last block.\n\n");
    void * first = _malloc(REGION_MIN_SIZE / 2);
    _malloc(REGION_MIN_SIZE / 2);
    _free(first);
    debug_heap(stdout, heap);
    print_padding();
    free_heap(heap, REGION_MIN_SIZE*2);
    print_test_successful();
}

void grow_somewhere_else(){
    void * heap = heap_init(RANDOM_INIT_SIZE);
    void * second_heap = heap_init_with_addr(heap+REGION_MIN_SIZE, RANDOM_INIT_SIZE);
    printf("Heap grow after another heap.\n\n");
    _malloc(REGION_MIN_SIZE / 2);
    _malloc(REGION_MIN_SIZE / 2);
    debug_heap(stdout, heap);
    print_padding();
    free_heap(heap, REGION_MIN_SIZE*2);
    free_heap(second_heap, REGION_MIN_SIZE);
    print_test_successful();
}


void heap_growth(){
    grow_near();
    grow_somewhere_else();
}